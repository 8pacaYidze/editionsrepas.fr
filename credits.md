---
title: Crédits
---

# Crédits

### Conception, web développement et réalisation

* [Thomas Parisot](https://détour.studio) { target=_blank rel="noopener noreferrer" }
* Valentine Porche

### Développement et méthodologie projet

- **Temps total** : {{ temps.total | inHours }} heures ({{ temps.total | inDays }} jours)
- Temps passé par **Sandra** : {{ temps.sandra | inHours }} heures ({{ temps.sandra | inDays }} jours)
- Temps passé par **Thomas** : {{ temps.thomas | inHours }} heures ({{ temps.thomas | inDays }} jours)
- Temps passé par **Valentine** : {{ temps.valentine | inHours }} heures ({{ temps.valentine | inDays }} jours)

Le [code source de ce site est en libre accès](https://framagit.org/reseau-repas/editionsrepas.fr).


### Services

- Hébergeur : [Cliss XXI](https://www.cliss21.com/site/) - 23 avenue Jean Jaurès, 62800 Liévin
- [FramaGit](https://framagit.org)
- [GitLab](https://gitlab.com/gitlab-org/gitlab)

### Images, multimédia

Images, illustrations, vidéos : Editions REPAS ou les crédits mentionnés dans les pages.

### Polices d'écriture utilisées

- 'Coline' par [A is for fonts](https://aisforfonts.com/coline)
- 'Caladea' par Andrés Torresi et Carolina Giovanolli
- 'Cocogoose' par Zetafonts
- 'Linux Libertine' par Philipp H. Poll
- 'Highvoltage rough' par Levi Szekeres
- 'Mermaid' par Scott Simpson

